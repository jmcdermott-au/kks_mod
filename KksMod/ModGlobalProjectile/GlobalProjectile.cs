﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace KksMod.ModGlobalNPC
{
    public class TestGlobalProjectile : GlobalProjectile
    {
        public override bool PreDraw(Projectile projectile, SpriteBatch spriteBatch, Color lightColor)
        {

            byte something4;
            byte something5;
            byte something6;

            if (projectile.hostile)
            {
                something4 = byte.MaxValue;
                something5 = 50;
                something6 = 50;

                if (lightColor.R < something4)
                {
                    lightColor.R = something4;
                }
                if (lightColor.G < something5)
                {
                    lightColor.G = something5;
                }
                if (lightColor.B < something6)
                {
                    lightColor.B = something6;
                }
            }
            
            return base.PreDraw(projectile, spriteBatch, lightColor);
        }

    }
}
