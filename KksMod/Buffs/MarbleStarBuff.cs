﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Terraria;
using Terraria.DataStructures;
using Terraria.ID;
using Terraria.ModLoader;

namespace KksMod.Buffs
{
	public class MarbleStarBuff : ModBuff
	{
		public override void SetDefaults()
		{
			DisplayName.SetDefault("Test Buff");
			Description.SetDefault("This does something.");
			Main.buffNoTimeDisplay[Type] = false;
			Main.debuff[Type] = false;
		}

        public override void Update(Player player, ref int buffIndex)
        {
            base.Update(player, ref buffIndex);
			player.statManaMax2 += 20;
        }

    }

}
