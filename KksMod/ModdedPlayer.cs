﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace KksMod
{
	public class ModdedPlayer : ModPlayer
	{
        static public int EffectTimerDefault = 600;
        public int EffectTimer = EffectTimerDefault;
        private Random rand = new Random();

        
        public override void PreUpdate()
        {
            //Countdown timer for Chaos mode
            if (EffectTimer > 0)
            {
                EffectTimer--;
                if (EffectTimer == 600 || EffectTimer == 500 || EffectTimer == 400 || EffectTimer == 300 || EffectTimer == 200 || EffectTimer == 100)
                {
                    Main.NewText($"{EffectTimer / 100}");
                }
            }
            else
            {   //Picking an effect for Chaos mode

                int X = rand.Next(20, 25);
                if (X == 0) //give random buff/debuff
                {
                    int BuffNumber = rand.Next(0, 100); 
                    player.AddBuff(BuffNumber, 600, true);
                    Main.NewText("Random Buff/Debuff");
                }
                if (X == 1) //spawn grenade
                {
                    Projectile.NewProjectile(player.position, player.velocity, ProjectileID.Grenade, 1, 10, player.whoAmI);
                    Main.NewText("Spawn Grenade");
                }
                if (X == 2) //give life crystal
                {
                    Item.NewItem(player.getRect(), ItemID.LifeCrystal, 1);
                    Main.NewText("Give Life Crystal");
                }
                if (X == 3) //become ninja
                {
                    Item.NewItem(player.getRect(), ItemID.NinjaHood, 1);
                    Item.NewItem(player.getRect(), ItemID.NinjaPants, 1);
                    Item.NewItem(player.getRect(), ItemID.NinjaShirt, 1);
                    Item.NewItem(player.getRect(), ItemID.Shuriken, 250);
                    Main.NewText("Become Ninja");
                }
                if (X == 4) //give mana crystal
                {
                    Item.NewItem(player.getRect(), ItemID.ManaCrystal);
                    Main.NewText("Give Mana Crystal");
                }
                if (X == 5) //spawn nine spiders
                {
                    Main.NewText("Are you afraid of spiders?");
                    for (int i = 0; i <= 4; i++)
                    {
                        NPC.NewNPC((int)player.position.X, (int)player.position.Y, NPCID.BlackRecluse);
                    }
                }
                if (X == 6) //big items
                {
                    Main.NewText("big items");
                    int k;
                    for (k = 1; k < 11; k++)
                    {
                        player.inventory[k].scale = 5f;
                    }
                }
                if (X == 7)
                {
                    Main.NewText("Make hotbar deadly");
                    int k;
                    for (k = 1; k < 11; k++) //reforge hotbar
                    {
                        player.inventory[k].prefix = PrefixID.Deadly;
                    }
                }
                EffectTimer = EffectTimerDefault; //Reset timer for Chaos mode
            }
        }
    }
}