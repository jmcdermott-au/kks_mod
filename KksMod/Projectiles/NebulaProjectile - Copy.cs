using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace KksMod.Projectiles
{
    public class TestProjectile : ModProjectile
    {

        public override string Texture => "Terraria/Item_" + ItemID.LaserRifle;
        public override void SetDefaults()
        {
            projectile.width = 40;
            projectile.height = 40; //sprite is 20 pixels tall
            projectile.friendly = true; //player projectile
            projectile.ranged = true; //ranged projectile
            projectile.timeLeft = 600; //lasts for 600 frames/ticks. Terraria runs at 60FPS, so it lasts 10 seconds.
            projectile.aiStyle = 1;
            projectile.penetrate = 10;
            projectile.tileCollide = false;
        }
        public override void AI()
        {
            Vector2 humbungus;
            humbungus.X = 0;
            humbungus.Y = -10;
            projectile.velocity = humbungus;
            
            projectile.ai[0] += 1f; // Use a timer to wait 15 ticks before applying gravity.
            if (projectile.ai[0] >= 15f)
            {

                humbungus.X += 1;
                humbungus.Y += 10;
                projectile.velocity = humbungus;
            }


        }
	}
}