using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace KksMod.Projectiles
{
	public class MagnetSphereTest : ModProjectile
	{

		public override void SetStaticDefaults()
		{
		}

		public override void SetDefaults()
		{
			projectile.width = 40;
			projectile.height = 40; //sprite is 20 pixels tall
			projectile.friendly = true; //player projectile
			projectile.ranged = true; //ranged projectile
			projectile.timeLeft = 600; //lasts for 600 frames/ticks. Terraria runs at 60FPS, so it lasts 10 seconds.
			projectile.aiStyle = 47;
			projectile.penetrate = 10;
			projectile.tileCollide = false;
		}


		public override void AI()
		{
			if (projectile.ai[0] == 0f)
			{
				projectile.ai[0] = projectile.velocity.X;
				projectile.ai[1] = projectile.velocity.Y;
			}
			if (projectile.velocity.X > 0f)
			{
				projectile.rotation += (Math.Abs(projectile.velocity.Y) + Math.Abs(projectile.velocity.X)) * 0.001f;
			}
			else
			{
				projectile.rotation -= (Math.Abs(projectile.velocity.Y) + Math.Abs(projectile.velocity.X)) * 0.001f;
			}

			if (Math.Sqrt(projectile.velocity.X * projectile.velocity.X + projectile.velocity.Y * projectile.velocity.Y) > 2.0)
			{
				projectile.velocity *= 0.98f;
			}
			for (int num566 = 0; num566 < 1000; num566++)
			{
				if (num566 != projectile.whoAmI && Main.projectile[num566].active && Main.projectile[num566].owner == projectile.owner && Main.projectile[num566].type == projectile.type && projectile.timeLeft > Main.projectile[num566].timeLeft && Main.projectile[num566].timeLeft > 30)
				{
					Main.projectile[num566].timeLeft = 30;
				}
			}
			int[] array = new int[20];
			int num567 = 0;
			float NPCRange = 3000f;
			bool AttackFlag = false;
			float ChosenNPCXValue = 0f;
			float ChosenNPCYValue = 0f;
			for (int num572 = 0; num572 < 200; num572++)
			{
				if (!Main.npc[num572].CanBeChasedBy(this))
				{
					continue;
				}
				float NPCXVALUE = Main.npc[num572].position.X + (float)(Main.npc[num572].width / 2);
				float NPCYVALUE = Main.npc[num572].position.Y + (float)(Main.npc[num572].height / 2);
				float ProjNpcDifference = Math.Abs(projectile.position.X + (float)(projectile.width / 2) - NPCXVALUE) + Math.Abs(projectile.position.Y + (float)(projectile.height / 2) - NPCYVALUE);
				if (ProjNpcDifference < NPCRange && Collision.CanHit(projectile.Center, 1, 1, Main.npc[num572].Center, 1, 1))
				{
					if (num567 < 20)
					{
						array[num567] = num572;
						num567++;
						ChosenNPCXValue = NPCXVALUE;
						ChosenNPCYValue = NPCYVALUE;
					}
					AttackFlag = true;
				}
			}
			if (projectile.timeLeft < 30)
			{
				AttackFlag = false;
			}
			if (AttackFlag)
			{
				int num576 = Main.rand.Next(num567);
				num576 = array[num576];
				ChosenNPCXValue = Main.npc[num576].position.X + (float)(Main.npc[num576].width / 2);
				ChosenNPCYValue = Main.npc[num576].position.Y + (float)(Main.npc[num576].height / 2);
				projectile.localAI[0] += 1f;
				if (projectile.localAI[0] > 7f) //The lower this value, the more often it shoots.
				{
					projectile.localAI[0] = 0f;
					float num577 = 16f; //Velocity multiplier
					Vector2 ProjectilePositionVector = new Vector2(projectile.position.X + (float)projectile.width * 0.5f, projectile.position.Y + (float)projectile.height * 0.5f);
					ProjectilePositionVector += projectile.velocity * 4f;
					float NpcXProjectileXDifference = ChosenNPCXValue - ProjectilePositionVector.X;
					float NpcYProjectileYDifference = ChosenNPCYValue - ProjectilePositionVector.Y;
					float num580 = (float)Math.Sqrt(NpcXProjectileXDifference * NpcXProjectileXDifference + NpcYProjectileYDifference * NpcYProjectileYDifference);
					float num581 = num580;
					num580 = num577 / num580;
					NpcXProjectileXDifference *= num580;
					NpcYProjectileYDifference *= num580;
					Projectile.NewProjectile(ProjectilePositionVector.X, ProjectilePositionVector.Y, NpcXProjectileXDifference, NpcYProjectileYDifference, ProjectileID.FlamingJack, projectile.damage, projectile.knockBack, projectile.owner);
				}
			}
		}
	}
}






