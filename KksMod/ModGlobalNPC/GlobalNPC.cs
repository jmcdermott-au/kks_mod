﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace KksMod.ModGlobalNPC
{
    public class TestGlobalNPC : GlobalNPC
    {

        public override void NPCLoot(NPC npc)
        {
            if (npc.type == NPCID.UmbrellaSlime)
            {
                if (Main.rand.NextFloat() < .10000f) // 13.23% chance
                    Item.NewItem(npc.getRect(), ItemID.Umbrella);
            }
        }

        public override void DrawEffects(NPC npc, ref Color drawColor)
        {
            base.DrawEffects(npc, ref drawColor);
            byte something;
            byte something2;
            byte something3;
            
            if (npc.friendly)
            {
                something = 50;
                something2 = byte.MaxValue;
                something3 = 50;

                if (drawColor.R < something)
                {
                    drawColor.R = something;    
                }
                if(drawColor.G < something2)
                {
                    drawColor.G = something2;
                }
                if (drawColor.B < something3)
                {
                    drawColor.B = something3;
                }
            }
        }
    }
}
