﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;
using KksMod.Items.Materials;
using KksMod.Items.Accessories;

namespace KksMod.Items
{
	[AutoloadEquip(EquipType.Head)]
	public class TestHelmet : ModItem
	{
		public override void SetStaticDefaults()
		{
			// DisplayName.SetDefault("WhatIsThis"); // By default, capitalization in classnames will add spaces to the display name. You can customize the display name here by uncommenting this line.
			Tooltip.SetDefault("");
		}

		public override void SetDefaults()
		{
			item.defense = 10;
			item.height = 24;
			item.width = 24;
		}

		public override void UpdateEquip(Player player)
		{
			player.accCalendar = true; 
			player.accCritterGuide = true; 
			player.accFishFinder = true; 
			player.accOreFinder = true; 
			player.accStopwatch = true; 
			player.accWeatherRadio = true;
			player.accDreamCatcher = true;
			player.accJarOfSouls = true;
			player.accThirdEye = true;
			player.blockRange += 15;
			player.findTreasure = true;

		}

		public override void AddRecipes()
        {
			ModRecipe recipe = new ModRecipe(mod);
			recipe.AddIngredient(ModContent.ItemType<TheConcoction>(), 5);
			recipe.AddIngredient(ModContent.ItemType<LihzarhdBeetle>());
			recipe.AddIngredient(ItemID.FragmentSolar, 3);
			recipe.AddIngredient(ItemID.FragmentNebula, 3);
			recipe.AddIngredient(ItemID.FragmentStardust, 3);
			recipe.AddIngredient(ItemID.FragmentVortex, 3);
			recipe.AddTile(TileID.LihzahrdAltar);
			recipe.AddTile(TileID.LunarCraftingStation);
			recipe.SetResult(this);
			recipe.AddRecipe();
        }
	}

}
