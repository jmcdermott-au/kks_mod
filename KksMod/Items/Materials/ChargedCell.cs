﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Terraria;
using Terraria.DataStructures;
using Terraria.ID;
using Terraria.ModLoader;

namespace KksMod.Items.Materials
{
	public class ChargedCell : ModItem
	{
		public override void SetStaticDefaults()
		{
			// DisplayName.SetDefault("WhatIsThis"); // By default, capitalization in classnames will add spaces to the display name. You can customize the display name here by uncommenting this line.
			Tooltip.SetDefault("Enough power to sustain a civilisation");
			DisplayName.SetDefault("Charged Lihzarhd Power Cell");
		}

		public override void SetDefaults()
		{
			item.height = 24;
			item.width = 24;
			item.useStyle = ItemUseStyleID.HoldingOut;
			item.useTime = 20;
			item.useAnimation = 20;
		}

        public override void AddRecipes()
        {
			ModRecipe recipe = new ModRecipe(mod);
			recipe.AddIngredient(ItemID.LihzahrdPowerCell);
			recipe.AddIngredient(ModContent.ItemType<TestMaterial>(), 3);
			recipe.AddTile(TileID.LihzahrdAltar);
			recipe.SetResult(this);
			recipe.AddRecipe();
		}
    }

}
