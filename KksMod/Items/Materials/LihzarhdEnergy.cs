﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Terraria;
using Terraria.DataStructures;
using Terraria.ID;
using Terraria.ModLoader;

namespace KksMod.Items.Materials
{
	public class LihzarhdEnergy : ModItem
	{
		public override void SetStaticDefaults()
		{
			// DisplayName.SetDefault("WhatIsThis"); // By default, capitalization in classnames will add spaces to the display name. You can customize the display name here by uncommenting this line.
			Tooltip.SetDefault("Enough power to sustain a civilisation");
		}

		public override void SetDefaults()
		{
			item.height = 24;
			item.width = 24;
		}
	}

}
