﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;
using KksMod.Buffs;
using KksMod;

namespace KksMod.Items.Materials
{
	public class TheConcoction : ModItem
	{
		public override void SetStaticDefaults()
		{
			// DisplayName.SetDefault("WhatIsThis"); // By default, capitalization in classnames will add spaces to the display name. You can customize the display name here by uncommenting this line.
			Tooltip.SetDefault("DO NOT DRINK."
				+ $"\n[i:1274]");
		}

		public override void SetDefaults()
		{
			item.height = 24;
			item.width = 24;
			item.useStyle = ItemUseStyleID.EatingUsing;
			item.useAnimation = 17;
			item.useTime = 17;
			item.consumable = true;
			item.UseSound = SoundID.Item3;
		}

		public override void AddRecipes()
        {
			ModRecipe recipe = new ModRecipe(mod);
			recipe.AddIngredient(ItemID.FlaskofGold);
			recipe.AddIngredient(ItemID.FlaskofFire);
			recipe.AddIngredient(ItemID.FlaskofCursedFlames);
			recipe.AddIngredient(ItemID.FlaskofNanites);
			recipe.AddIngredient(ItemID.FlaskofParty);
			recipe.AddIngredient(ItemID.FlaskofPoison);
			recipe.AddIngredient(ItemID.FlaskofVenom);
			recipe.AddTile(TileID.Bathtubs);
			recipe.SetResult(this);
			recipe.AddRecipe();
        }

		public override bool UseItem(Player player)
        {
			player.statLifeMax2 = 1;
			return true;
        }
       
        }

	}


