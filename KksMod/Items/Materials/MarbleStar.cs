﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;
using KksMod.Buffs;

namespace KksMod.Items.Materials
{
	public class MarbleStar : ModItem
	{
		public override void SetStaticDefaults()
		{
			// DisplayName.SetDefault("WhatIsThis"); // By default, capitalization in classnames will add spaces to the display name. You can customize the display name here by uncommenting this line.
			Tooltip.SetDefault("Is marble a crystal?");
		}

		public override void SetDefaults()
		{
			item.height = 24;
			item.width = 24;
			item.consumable = true;
			item.buffType = ModContent.BuffType<MarbleStarBuff>();
			item.buffTime = 6000;
			item.useStyle = ItemUseStyleID.EatingUsing;
			item.useTime = 17;
			item.useAnimation = 17;
		}

		public override void AddRecipes()
		{
			ModRecipe recipe = new ModRecipe(mod);
			recipe.AddIngredient(ItemID.ManaCrystal);
			recipe.AddIngredient(ItemID.Marble, 35);
			recipe.AddTile(TileID.MythrilAnvil);
			recipe.SetResult(this);
			recipe.AddRecipe();
		}

	}

}
