﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace KksMod.Items.Materials
{
	public class DeathFruit : ModItem
	{
		public override void SetStaticDefaults()
		{
			// DisplayName.SetDefault("WhatIsThis"); // By default, capitalization in classnames will add spaces to the display name. You can customize the display name here by uncommenting this line.
			Tooltip.SetDefault("Permanently increases cancer risk by 5");
		}

		public override void SetDefaults()
		{
			item.height = 24;
			item.width = 24;
			item.useStyle = ItemUseStyleID.EatingUsing;
			item.useAnimation = 17;
			item.useTime = 17;
			item.consumable = true;
			item.UseSound = SoundID.Item3;
		}

		public override void AddRecipes()
        {
			ModRecipe recipe = new ModRecipe(mod);
			recipe.AddIngredient(ItemID.LifeFruit);
			recipe.AddIngredient(ItemID.SoulofNight);
			recipe.AddIngredient(ItemID.Ectoplasm);
			recipe.AddTile(TileID.DemonAltar);
			recipe.SetResult(this);
			recipe.AddRecipe();
        }

		public override bool UseItem(Player player)
        {
			player.AddBuff(BuffID.Suffocation, 600);
			player.AddBuff(BuffID.Bleeding, 600);
			player.AddBuff(BuffID.Darkness, 600);
			player.AddBuff(BuffID.Poisoned, 600);
			player.AddBuff(BuffID.PotionSickness, 600);
			player.AddBuff(BuffID.Slow, 600);
			player.AddBuff(BuffID.Venom, 600);
			return true;
        }
        public override void OnConsumeItem(Player player)
        {
            player.AddBuff(BuffID.Suffocation, 600);

        }

	}

}
