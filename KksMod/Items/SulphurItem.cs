using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace KksMod.Items
{
	public class SulphurItem : ModItem
	{
		public override void SetStaticDefaults() 
		{
			// DisplayName.SetDefault("WhatIsThis"); // By default, capitalization in classnames will add spaces to the display name. You can customize the display name here by uncommenting this line.
			Tooltip.SetDefault("It's sulphur.");
			DisplayName.SetDefault("Sulphur");
		}
		       public override void SetDefaults()
        {
			item.width = 12;
			item.height = 12;
			item.rare = ItemRarityID.Blue;
			item.maxStack = 99;
			item.value = Item.sellPrice(0, 0, 10);
			item.createTile = ModContent.TileType<Tiles.SulphurOre>();
			item.placeStyle = 0;
			item.useAnimation = 15;
			item.useTime = 15;
			item.autoReuse = true;
			item.useStyle = ItemUseStyleID.SwingThrow;
			item.consumable = true;
		}
    }
}