using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace KksMod.Items
{
	public class AmmunitionCrate : ModItem
	{
		private Random rand = new Random();

		public override void SetStaticDefaults() 
		{
			// DisplayName.SetDefault("WhatIsThis"); // By default, capitalization in classnames will add spaces to the display name. You can customize the display name here by uncommenting this line.
			Tooltip.SetDefault("Makes a rattling sound when shook");
			DisplayName.SetDefault("Ammunition crate");
		}
		       public override void SetDefaults()
        {
			item.width = 12;
			item.height = 12;
			item.rare = ItemRarityID.Blue;
			item.maxStack = 99;
			item.value = Item.sellPrice(0, 0, 10);
			item.createTile = TileID.FishingCrate;
			item.placeStyle = 0;
			item.useAnimation = 15;
			item.useTime = 15;
			item.autoReuse = true;
			item.useStyle = ItemUseStyleID.SwingThrow;
			item.consumable = true;
		}
						
		public override void AddRecipes() 
		{
			ModRecipe recipe = new ModRecipe(mod);
			recipe.AddIngredient(ItemID.LargeDiamond, 3);
			recipe.AddTile(TileID.CrystalBall);
			recipe.AddIngredient(ItemID.PlatinumBar, 30);
			recipe.SetResult(this);
			recipe.AddRecipe();
		}

		public override bool CanRightClick()
        {
			return true;
        }

        public override void RightClick(Player player)
        {
			int X = rand.Next(0, 20);
			switch (X)
            {
				case 1:
					Item.NewItem(player.getRect(), ItemID.ChlorophyteBullet, 999);
					break;

				case 2:
					Item.NewItem(player.getRect(), ItemID.MusketBall, 999);
					break;

				case 3:
					Item.NewItem(player.getRect(), ItemID.MeteorShot, 333);
					break;

				case 4:
					Item.NewItem(player.getRect(), ItemID.SilverBullet, 333);
					break;

				case 5:
					Item.NewItem(player.getRect(), ItemID.CrystalBullet, 333);
					break;

				case 6:
					Item.NewItem(player.getRect(), ItemID.CursedBullet, 333);
					break;

				case 7:
					Item.NewItem(player.getRect(), ItemID.HighVelocityBullet, 222);
					break;

				case 8:
					Item.NewItem(player.getRect(), ItemID.IchorBullet, 222);
					break;

				case 9:
					Item.NewItem(player.getRect(), ItemID.VenomBullet, 222);
					break;

				case 10:
					Item.NewItem(player.getRect(), ItemID.PartyBullet, 222);
					break;

				case 11:
					Item.NewItem(player.getRect(), ItemID.NanoBullet, 111);
					break;

				case 12:
					Item.NewItem(player.getRect(), ItemID.ExplodingBullet, 111);
					break;

				case 13:
					Item.NewItem(player.getRect(), ItemID.GoldenBullet, 111);
					break;

				case 14:
                    Item.NewItem(player.getRect(), ItemID.MoonlordBullet, 50);
					break;

				case 15:
					Item.NewItem(player.getRect(), ItemID.StyngerBolt, 250);
					break;

				case 16:
					Item.NewItem(player.getRect(), ItemID.CandyCorn, 250);
                    break;

				case 17:
					Item.NewItem(player.getRect(), ItemID.ExplosiveJackOLantern, 250);
					break;

				case 18:
					Item.NewItem(player.getRect(), ItemID.Stake, 250);
					break;

				case 19:
					Item.NewItem(player.getRect(), ItemID.Nail, 250);
					break;

				case 20:
					Item.NewItem(player.getRect(), ItemID.EndlessQuiver);
					break;

				case 21:
					Item.NewItem(player.getRect(), ItemID.FlamingArrow, 999);
					break;

				case 22:
					Item.NewItem(player.getRect(), ItemID.UnholyArrow, 666);
					break;

				case 23:
					Item.NewItem(player.getRect(), ItemID.JestersArrow, 444);
					break;

				case 24:
					Item.NewItem(player.getRect(), ItemID.HellfireArrow, 444);
					break;

				case 25:
					Item.NewItem(player.getRect(), ItemID.HolyArrow, 333);
					break;

				case 26:
					Item.NewItem(player.getRect(), ItemID.CursedArrow, 333);
					break;

				case 27:
					Item.NewItem(player.getRect(), ItemID.FrostburnArrow, 333);
					break;
            }
        }
    }
}