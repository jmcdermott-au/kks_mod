using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;
using KksMod.Items.Materials;

namespace KksMod.Items
{
	public class UraniumHammer : ModItem
	{
		public override void SetStaticDefaults() 
		{
			// DisplayName.SetDefault("WhatIsThis"); // By default, capitalization in classnames will add spaces to the display name. You can customize the display name here by uncommenting this line.
			Tooltip.SetDefault("the skin on my hand is melting");
        }

        public override string Texture => "Terraria/Item_" + ItemID.ChlorophyteWarhammer;


        public override void SetDefaults()
		{
			item.CloneDefaults(ItemID.ChlorophyteWarhammer);
			item.value = 20000;
			item.useStyle = ItemUseStyleID.SwingThrow;
			item.hammer = 10000;
			item.useTime = 1;
			item.useAnimation = 5;
			item.damage = 10;
		}

		public override void AddRecipes() 
		{
			ModRecipe recipe = new ModRecipe(mod);
			recipe.AddIngredient(ItemID.ChlorophyteWarhammer);
			recipe.AddIngredient(ModContent.ItemType<ChargedCell>());
			recipe.AddTile(TileID.CrystalBall);
			recipe.AddTile(TileID.MythrilAnvil);
			recipe.AddIngredient(ModContent.ItemType<UraniumOre>(), 5);
			recipe.SetResult(this);
			recipe.AddRecipe();
		}
    }
}