using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace KksMod.Items.Weapons
{
	public class NebulaTome : ModItem
	{
		public override void SetStaticDefaults() 
		{
			// DisplayName.SetDefault("WhatIsThis"); // By default, capitalization in classnames will add spaces to the display name. You can customize the display name here by uncommenting this line.
			Tooltip.SetDefault("She's de-fanged and completely harmless. Mostly.");
		}

		public override void SetDefaults() 
		{
			item.damage = 3000;
			item.magic = true;
			item.noMelee = true;
			item.width = 40;
			item.height = 40;
			item.useTime = 20;
			item.useAnimation = 20;
			item.knockBack = 6;
			item.value = 10000;
			item.rare = ItemRarityID.Purple;
			item.UseSound = SoundID.Item29;
			item.autoReuse = true;
			item.shoot = mod.ProjectileType("NebulaProjectile");
			item.shootSpeed = 25;
			item.useStyle = ItemUseStyleID.HoldingUp;
			item.mana = 4;
		}

		public override bool Shoot(Player player, ref Microsoft.Xna.Framework.Vector2 position, ref float speedX, ref float speedY, ref int type, ref int damage, ref float knockBack)

		{
			float numberProjectiles = 36; // 3 shots
			float rotation = MathHelper.ToRadians(180);//Shoots them in a 45 degree radius. (This is technically 90 degrees because it's 45 degrees up from your cursor and 45 degrees down)
			position += Vector2.Normalize(new Vector2(speedX, speedY)) * 0f; //45 should equal whatever number you had on the previous line
			for (int i = 0; i < numberProjectiles; i++)
			{
				Vector2 perturbedSpeed = new Vector2(speedX, speedY).RotatedBy(MathHelper.Lerp(-rotation, rotation, i / (numberProjectiles - 1))) * .2f; // Vector for spread. Watch out for dividing by 0 if there is only 1 projectile.
				Projectile.NewProjectile(position.X, position.Y, perturbedSpeed.X, perturbedSpeed.Y, type, damage, knockBack, player.whoAmI); //Creates a new projectile with our new vector for spread.
			}
			return false; //makes sure it doesn't shoot the projectile again after this
		}

		public override void AddRecipes() 
		{
			ModRecipe recipe = new ModRecipe(mod);
			recipe.AddIngredient(ItemID.DarkShard, 3);
			recipe.AddIngredient(ItemID.SpellTome, 1);
			recipe.AddIngredient(ItemID.SoulofNight, 3);
			recipe.AddTile(TileID.Bookcases);
			recipe.AddTile(TileID.CrystalBall);
			recipe.SetResult(this);
			recipe.AddRecipe();
		}
	}
}