using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace KksMod.Items.Weapons
{
	public class MaxRegen : ModItem
	{

		public int TimerValue = 0;


		public override void UpdateInventory(Player player)
        {
			if (player.whoAmI == Main.myPlayer && TimerValue != 0 )
			{
				TimerValue -= 1;
				player.wingTime += (TimerValue / 100);
			}
		}

		public override void OnHitNPC(Player player, NPC target, int damage, float knockBack, bool crit)
        {
			if (player.whoAmI == Main.myPlayer)
            {
				TimerValue += 30;
            }

		}


		public override void SetStaticDefaults() 
		{
			// DisplayName.SetDefault("WhatIsThis"); // By default, capitalization in classnames will add spaces to the display name. You can customize the display name here by uncommenting this line.
			Tooltip.SetDefault("CUSTOM DATA TEST");
		}

		public override void SetDefaults()
		{
			item.damage = 200;
			item.melee = true;
			item.noMelee = false;
			item.width = 10;
			item.height = 10;
			item.useTime = 10;
			item.useAnimation = 10;
			item.useStyle = 1;
			item.knockBack = 60;
			item.value = 10000;
			item.rare = 2;
			item.UseSound = SoundID.Item1;
			item.autoReuse = true;
			item.scale = 3f;
			item.crit = 100;
		}

		public override void AddRecipes() 
		{
			ModRecipe recipe = new ModRecipe(mod);
			recipe.AddIngredient(1527, 3);
			recipe.AddTile(125);
			recipe.AddIngredient(706, 30);
			recipe.SetResult(this);
			recipe.AddRecipe();
		}
	}
}