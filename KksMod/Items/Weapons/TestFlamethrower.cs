using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace KksMod.Items.Weapons
{
	public class TestFlamethrower : ModItem
	{
		public override void SetStaticDefaults() 
		{
			// DisplayName.SetDefault("WhatIsThis"); // By default, capitalization in classnames will add spaces to the display name. You can customize the display name here by uncommenting this line.
			Tooltip.SetDefault("CUSTOM DATA TEST");
		}

		public override void SetDefaults()
		{
			item.damage = 200;
			item.melee = true;
			item.noMelee = false;
			item.width = 10;
			item.height = 10;
			item.useTime = 10;
			item.useAnimation = 10;
			item.useStyle = ItemUseStyleID.SwingThrow;
			item.knockBack = 60;
			item.value = 10000;
			item.rare = 2;
			item.UseSound = SoundID.Item1;
			item.autoReuse = true;
			item.scale = 3f;
			item.crit = 100;
		}

		public override void AddRecipes() 
		{
			ModRecipe recipe = new ModRecipe(mod);
			recipe.AddIngredient(ItemID.LargeDiamond, 3);
			recipe.AddTile(TileID.CrystalBall);
			recipe.AddIngredient(ItemID.PlatinumBar, 30);
			recipe.SetResult(this);
			recipe.AddRecipe();
		}
	}
}
