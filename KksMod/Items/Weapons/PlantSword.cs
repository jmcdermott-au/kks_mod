using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace KksMod.Items.Weapons
{
	public class PlantSword : ModItem
	{
		public override void SetStaticDefaults() 
		{
			// DisplayName.SetDefault("WhatIsThis"); // By default, capitalization in classnames will add spaces to the display name. You can customize the display name here by uncommenting this line.
			Tooltip.SetDefault("Vegan-Friendly");
		}

		public override void SetDefaults()
		{
			item.damage = 42;
			item.melee = true;
			item.noMelee = false;
			item.width = 10;
			item.height = 10;
			item.useTime = 10;
			item.useAnimation = 10;
			item.useStyle = ItemUseStyleID.SwingThrow;
			item.knockBack = 6;
			item.value = 10000;
			item.rare = ItemRarityID.Green;
			item.UseSound = SoundID.Item1;
			item.autoReuse = true;
			item.shootSpeed = 25;
			item.shoot = ProjectileID.CrystalLeafShot;
			item.scale = 2f;
		}

		public override bool Shoot(Player player, ref Microsoft.Xna.Framework.Vector2 position, ref float speedX, ref float speedY, ref int type, ref int damage, ref float knockBack)

        {
			Projectile.NewProjectile(position.X, position.Y, speedX, speedY, ProjectileID.CrystalStorm, damage / 2, knockBack, player.whoAmI);	
			return true;
		}

		public override void AddRecipes() 
		{
			ModRecipe recipe = new ModRecipe(mod);
			recipe.AddIngredient(ItemID.ChlorophyteClaymore);
			recipe.AddIngredient(ItemID.TrueExcalibur);
			recipe.AddIngredient(ItemID.SoulofLight, 3);
			recipe.AddIngredient(ItemID.SoulofSight, 8);
			recipe.AddTile(TileID.DemonAltar);
			recipe.SetResult(this);
			recipe.AddRecipe();
		}
	}
}