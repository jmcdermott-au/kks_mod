using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace KksMod.Items.Weapons
{
	public class RodFlying : ModItem
	{
		public override void SetStaticDefaults() 
		{
			// DisplayName.SetDefault("WhatIsThis"); // By default, capitalization in classnames will add spaces to the display name. You can customize the display name here by uncommenting this line.
			Tooltip.SetDefault("@everyone shut up.");
			DisplayName.SetDefault("Rod of Flight");
		}

		
		public override void SetDefaults()
		{
			item.damage = 1;
			item.melee = true;
			item.noMelee = false;
			item.width = 10;
			item.height = 10;
			item.useTime = 60;
			item.useAnimation = 60;
			item.useStyle = ItemUseStyleID.SwingThrow;
			item.knockBack = 0;
			item.value = 10000;
			item.rare = ItemRarityID.Green;
			item.UseSound = SoundID.Item1;
			item.autoReuse = true;
			item.scale = 2f;
		}

		public override void OnHitNPC(Player player, NPC target, int damage, float knockBack, bool crit)
        {
			int Time = player.wingTimeMax;

			if (crit == true)
                
			{
				player.wingTime += (Time / 3);
            }
			else
            {
				player.wingTime += (Time / 5);
            }
		}

		//The plan is to make this a rare drop from wyverns as well, so it's not fucking impossible to find. FUCK the rod of discord bro. Shit's ass.
		public override void AddRecipes() 
		{
			ModRecipe recipe = new ModRecipe(mod);
            recipe.AddIngredient(ItemID.RodofDiscord);
			recipe.AddIngredient(ItemID.Cloud, 50);
			recipe.AddIngredient(ItemID.SoulofFlight, 15);
			recipe.AddTile(TileID.SkyMill);
			recipe.SetResult(this);
			recipe.AddRecipe();
		}
	}
}