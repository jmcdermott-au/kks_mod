using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;
using KksMod.Projectiles;

namespace KksMod.Items.Weapons
{
	public class JackOfAllTrades : ModItem
	{
		public override void SetStaticDefaults() 
		{
			// DisplayName.SetDefault("WhatIsThis"); // By default, capitalization in classnames will add spaces to the display name. You can customize the display name here by uncommenting this line.
			Tooltip.SetDefault("Still better than a master of one.");
		}

		public override void SetDefaults()
		{
			item.damage = 20;
			item.melee = true;
			item.noMelee = true;
			item.width = 10;
			item.height = 10;
			item.useTime = 60;
			item.useAnimation = 60;
			item.useStyle = ItemUseStyleID.SwingThrow;
			item.knockBack = 60;
			item.value = 10000;
			item.rare = ItemRarityID.Yellow;
			item.UseSound = SoundID.Item1;
			item.autoReuse = false;
			item.crit = 100;
			item.shoot = mod.ProjectileType("MagnetSphereTest");
			item.shootSpeed = 10;
		}

		public override void AddRecipes() 
		{
			ModRecipe recipe = new ModRecipe(mod);
			recipe.AddIngredient(ItemID.LargeDiamond, 3);
			recipe.AddTile(TileID.CrystalBall);
			recipe.AddIngredient(ItemID.PlatinumBar, 30);
			recipe.SetResult(this);
			recipe.AddRecipe();
		}

        public override void MeleeEffects(Player player, Rectangle hitbox)
        {
			base.MeleeEffects(player, hitbox);
			Dust.NewDust(player.position, player.width, player.height, DustID.Adamantine, 0f, 0f, 200, default(Color), 0.8f);
        }
    }
}