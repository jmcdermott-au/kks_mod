using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;
using KksMod.Items.Materials;

namespace KksMod.Items.Weapons
{
	public class PlasmaGex : ModItem
	{
		public override void SetStaticDefaults() 
		{
			// DisplayName.SetDefault("WhatIsThis"); // By default, capitalization in classnames will add spaces to the display name. You can customize the display name here by uncommenting this line.
			Tooltip.SetDefault($"\n[c/00FF00:It's tail time.]");

		}


		public override string Texture => "Terraria/Item_" + ItemID.LaserRifle;
			
		public override void SetDefaults() 
		{
			item.damage = 45;
			item.reuseDelay = 14;
			item.magic = true;
			item.noMelee = true;
			item.width = 40;
			item.height = 40;
			item.useTime = 4;
			item.useAnimation = 12;
			item.knockBack = 6;
			item.value = 10000;
			item.rare = 2;
			item.UseSound = SoundID.Item31;
			item.autoReuse = true;
			item.shoot = ProjectileID.ChlorophyteBullet;
			item.shootSpeed = 7.75f;
			item.useStyle = ItemUseStyleID.HoldingOut;
			item.mana = 8;
		}

		public override bool Shoot(Player player, ref Microsoft.Xna.Framework.Vector2 position, ref float speedX, ref float speedY, ref int type, ref int damage, ref float knockBack)

		{
			Projectile.NewProjectile(position.X, position.Y, speedX, speedY, ProjectileID.ChlorophyteOrb, damage * 3, knockBack, player.whoAmI);
			Dust.NewDust(position, 5, 5, DustID.Chlorophyte, speedX, speedY, 0, default, 1);
			return true;
		}



		public override void AddRecipes() 
		{
			ModRecipe recipe = new ModRecipe(mod);
			recipe.AddIngredient(ItemID.DarkShard, 3);
			recipe.AddIngredient(ItemID.SpellTome, 1);
			recipe.AddIngredient(ItemID.SoulofNight, 3);
			recipe.AddTile(101);
			recipe.AddTile(125);
			recipe.SetResult(this);
			recipe.AddRecipe();
		}
	}
}