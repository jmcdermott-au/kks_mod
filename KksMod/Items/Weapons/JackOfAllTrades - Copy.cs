using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;
using KksMod.Projectiles;

namespace KksMod.Items.Weapons
{
	public class TestingWeapon : ModItem
	{
		public override void SetStaticDefaults() 
		{
			// DisplayName.SetDefault("WhatIsThis"); // By default, capitalization in classnames will add spaces to the display name. You can customize the display name here by uncommenting this line.
			Tooltip.SetDefault("Still better than a master of one.");
		}
		public override string Texture => "Terraria/Item_" + ItemID.LaserRifle;

		public override void SetDefaults()
		{
			item.CloneDefaults(ItemID.WaterBolt);
			item.shoot = mod.ProjectileType("TestProjectile");
		}

		public override void AddRecipes() 
		{
			ModRecipe recipe = new ModRecipe(mod);
			recipe.AddIngredient(ItemID.LargeDiamond, 3);
			recipe.AddTile(TileID.CrystalBall);
			recipe.AddIngredient(ItemID.PlatinumBar, 30);
			recipe.SetResult(this);
			recipe.AddRecipe();
		}


    }
}