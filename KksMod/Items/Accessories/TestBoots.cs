﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace KksMod.Items.Accessories
{
	public class TestBoots : ModItem
	{
		public override void SetStaticDefaults()
		{
			// DisplayName.SetDefault("WhatIsThis"); // By default, capitalization in classnames will add spaces to the display name. You can customize the display name here by uncommenting this line.
			Tooltip.SetDefault("Test Boots");
		}

		public override void SetDefaults()
		{
			item.accessory = true;
			item.height = 24;
			item.width = 24;
		}

		public override void UpdateAccessory(Player player, bool hideVisual)
        {
			player.endurance = 1;
			player.meleeCrit = 100;
			
        }

        }

	}


