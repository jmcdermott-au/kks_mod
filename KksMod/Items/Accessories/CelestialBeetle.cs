﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;
using KksMod.Items.Materials;

namespace KksMod.Items.Accessories
{
	public class CelestialBeetle : ModItem
	{
		public override void SetStaticDefaults()
		{
			// DisplayName.SetDefault("WhatIsThis"); // By default, capitalization in classnames will add spaces to the display name. You can customize the display name here by uncommenting this line.
			Tooltip.SetDefault("That is one powerful fucking beetle.");
		}

		public override void SetDefaults()
		{
			item.accessory = true;
			item.height = 24;
			item.width = 24;
		}

		public override void UpdateAccessory(Player player, bool hideVisual)
		{
			player.statLifeMax2 += 1000;
			player.maxMinions += 15;
			player.minionDamage += 5f;
		}

		public override void AddRecipes()
		{
			ModRecipe recipe = new ModRecipe(mod);
			recipe.AddIngredient(ModContent.ItemType<TheConcoction>(), 5);
			recipe.AddIngredient(ModContent.ItemType<LihzarhdBeetle>());
			recipe.AddIngredient(ItemID.FragmentSolar, 3);
			recipe.AddIngredient(ItemID.FragmentNebula, 3);
			recipe.AddIngredient(ItemID.FragmentStardust, 3);
			recipe.AddIngredient(ItemID.FragmentVortex, 3);
			recipe.AddTile(TileID.LihzahrdAltar);
			recipe.AddTile(TileID.LunarCraftingStation);
			recipe.SetResult(this);
			recipe.AddRecipe();
		}
	}

}
