﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace KksMod.Items.Accessories
{
	public class StarEmblem : ModItem
	{
		public override void SetStaticDefaults()
		{
			// DisplayName.SetDefault("WhatIsThis"); // By default, capitalization in classnames will add spaces to the display name. You can customize the display name here by uncommenting this line.
			Tooltip.SetDefault("It's good to be vulnerable sometimes.");
		}

		public override void SetDefaults()
		{
			item.accessory = true;
			item.height = 24;
			item.width = 24;
		}

		public override void UpdateAccessory(Player player, bool hideVisual)
		{
			player.statDefense -= 5;
			player.manaCost -= 0.2f;
			player.statManaMax2 += 35;
			player.magicCrit += 3;
			player.magicDamage += 3;
			player.manaRegen += 3;

		}

        public override void AddRecipes()
        {
            base.AddRecipes();
			ModRecipe recipe = new ModRecipe(mod);
			recipe.AddIngredient(ItemID.FallenStar, 15);
			recipe.AddIngredient(ItemID.ManaCrystal, 3);
			recipe.AddIngredient(ItemID.GreaterManaPotion, 5);
			recipe.AddIngredient(ItemID.MagicPowerPotion, 10);
			recipe.AddIngredient(ItemID.CelestialEmblem);
			recipe.AddTile(TileID.CrystalBall);
			recipe.SetResult(this);
			recipe.AddRecipe();
        }


    }

}
