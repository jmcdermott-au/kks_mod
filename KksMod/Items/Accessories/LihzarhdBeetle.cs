﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;
using KksMod.Items.Materials;

namespace KksMod.Items.Accessories
{
	public class LihzarhdBeetle : ModItem
	{
		public override void SetStaticDefaults()
		{
			// DisplayName.SetDefault("WhatIsThis"); // By default, capitalization in classnames will add spaces to the display name. You can customize the display name here by uncommenting this line.
			Tooltip.SetDefault("Increases maximum life by 50..... At a cost.");
		}

		public override void SetDefaults()
		{
			item.accessory = true;
			item.height = 24;
			item.width = 24;
		}

		public override void UpdateAccessory(Player player, bool hideVisual)
		{
			player.statLifeMax2 += 300;
			player.maxMinions += 3;
			player.minionDamage += 1f;
		}

		public override void AddRecipes()
        {
			ModRecipe recipe = new ModRecipe(mod);
			recipe.AddIngredient(ModContent.ItemType<TheConcoction>(), 5);
			recipe.AddIngredient(ItemID.LifeFruit, 5);
			recipe.AddIngredient(ItemID.HerculesBeetle);
			recipe.AddIngredient(ItemID.ChlorophyteOre, 150);
			recipe.AddIngredient(ItemID.Moonglow, 75);
			recipe.AddIngredient(ItemID.LargeEmerald, 3);
			recipe.AddTile(TileID.LihzahrdAltar);
			recipe.SetResult(this);
			recipe.AddRecipe();
        }
	}

}
