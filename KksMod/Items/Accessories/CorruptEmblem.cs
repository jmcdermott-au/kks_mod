﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;

namespace KksMod.Items.Accessories
{
	public class CorruptEmblem : ModItem
	{
		public override void SetStaticDefaults()
		{
			// DisplayName.SetDefault("WhatIsThis"); // By default, capitalization in classnames will add spaces to the display name. You can customize the display name here by uncommenting this line.
			Tooltip.SetDefault("Increases maximum life by 50..... At a cost.");
		}

		public override void SetDefaults()
		{
			item.accessory = true;
			item.height = 24;
			item.width = 24;
		}

		public override void UpdateAccessory(Player player, bool hideVisual)
		{
			player.statLifeMax2 += 50;
			player.meleeDamage -= 0.1f;
			player.meleeSpeed -= 0.1f;
			player.meleeCrit -= 7;

		}

	}

}
